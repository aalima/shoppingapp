//
//  ViewController.swift
//  onlineShoppingApp
//
//  Created by Alima Aglakova on 04/02/2021.
//


import UIKit

class ViewController: UIViewController{

    @IBOutlet weak var gridView: UICollectionView!
    @IBOutlet weak var tableListView: UITableView!
    @IBOutlet weak var mySwitch: UISegmentedControl!
    
    var products : [Product] = []
    var cart : [Product] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // table List View -->
        tableListView.delegate = self
        tableListView.dataSource = self
        self.ProductDetails()
        tableListView.isHidden = false
        gridView.isHidden = true
        tableListView.register(MyTableViewCell.nib, forCellReuseIdentifier: MyTableViewCell.identifier)
        
        
        // collection View -->
        self.gridView.dataSource = self
        self.gridView.delegate = self
        self.gridView.register(MyCollectionViewCell.nib, forCellWithReuseIdentifier: MyCollectionViewCell.identifier)
        
    }

    @IBAction func didTapSwitch(_ sender: Any) {
        // make inverse --> should always work properly, but i am not certain
        gridView.isHidden = !gridView.isHidden
        tableListView.isHidden = !tableListView.isHidden
    }
    
    @IBAction func didTapOnCart(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(identifier: CartViewController.identifier) as? CartViewController else{
                    return
            }
        vc.cart_items = self.cart
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func ProductDetails(){
        let product1 = Product(ImageSource: "hoodie", Title: "Hoodie", Description: "Black casual hoodie", Price: 15900)
        self.products.append(product1)
        
        let product2 = Product(ImageSource: "jumper", Title: "Jumper", Description: "Striped colourful jumper", Price: 19800)
        self.products.append(product2)
        
        let product3 = Product(ImageSource: "dress", Title: "Dress", Description: "A white classic dress", Price: 20900)
        self.products.append(product3)
        
        let product4 = Product(ImageSource: "shorts", Title: "Shorts", Description: "Simple gray shorts", Price: 12500)
        self.products.append(product4)
        
        let product5 = Product(ImageSource: "trousers", Title: "Trousers", Description: "Black classic trousers", Price: 16760)
        self.products.append(product5)
        
        let product6 = Product(ImageSource: "jumpsuit", Title: "Jumpsuit", Description: "Light blue jumpsuit", Price: 20000)
        self.products.append(product6)
        
        let product7 = Product(ImageSource: "shoes", Title: "Shoes", Description: "Modern boots with pattern", Price: 18700)
        self.products.append(product7)
        
        let product8 = Product(ImageSource: "sneakers", Title: "Sneakers", Description: "All white sneakers", Price: 17120)
        self.products.append(product8)
    }
}

protocol ViewControllerDelegate {
    func addToCart(_ item: Int)
}

extension ViewController: ViewControllerDelegate{
    func addToCart(_ item: Int) {
        let product = products[item]
        cart.append(product)
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(identifier: InfoViewController.identifier) as? InfoViewController else{
                    return
            }
        tableView.deselectRow(at: indexPath, animated: true)
        vc.item = self.products[indexPath.row]
        vc.index = indexPath.row
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(150)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyTableViewCell.identifier, for: indexPath) as! MyTableViewCell
        let item = self.products[indexPath.row]
        cell.myTableCellTitle.text = item.Title
        cell.myTableCellPrice.text = String(item.Price)
        cell.MyTableCellDescription.text = item.Description
        cell.myTableCellImage.image = UIImage(named: item.ImageSource)
        cell.index = indexPath.row
        cell.delegate = self
        return cell
    }

}

extension ViewController :UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = gridView.dequeueReusableCell(withReuseIdentifier: MyCollectionViewCell.identifier, for: indexPath) as! MyCollectionViewCell
        let item = self.products[indexPath.item]
        cell.myCollectionTitle.text = item.Title
        cell.myCollectionPrice.text = String(item.Price)
        cell.myCollectionDescription.text = item.Description
        cell.myCollectionImage.image = UIImage(named: item.ImageSource)
        cell.index = indexPath.row
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(identifier: InfoViewController.identifier) as? InfoViewController
        else{
            return
        }
        gridView.deselectItem(at: indexPath, animated: true)
        vc.item = self.products[indexPath.row]
        vc.index = indexPath.row
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.bounds.width/2
        return CGSize(width: width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
