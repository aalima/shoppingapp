//
//  CartViewController.swift
//  onlineShoppingApp
//
//  Created by Alima Aglakova on 06/02/2021.
//

import UIKit

class CartViewController: UIViewController{
    static let identifier = String(describing: CartViewController.self)
    @IBOutlet weak var cartTableView: UITableView!
    var cart_items: [Product] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        cartTableView.delegate = self
        cartTableView.dataSource = self
        cartTableView.register(MyTableViewCell.nib, forCellReuseIdentifier: MyTableViewCell.identifier)
    }
    
    @IBAction func didTapBuy(_ sender: Any) {
        let alert = UIAlertController(title: "Thank you!", message: "Your order has been successful!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Okay", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
            self.cart_items = []
            self.cartTableView.reloadData()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension CartViewController :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart_items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(150)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyTableViewCell.identifier, for: indexPath) as! MyTableViewCell
        let item = self.cart_items[indexPath.row]
        cell.myTableCellTitle.text = item.Title
        cell.myTableCellPrice.text = String(item.Price)
        cell.MyTableCellDescription.text = item.Description
        cell.myTableCellImage.image = UIImage(named: item.ImageSource)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(identifier: InfoViewController.identifier) as? InfoViewController
        else{
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        vc.item = self.cart_items[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}
