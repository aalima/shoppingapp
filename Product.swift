//
//  Product.swift
//  onlineShoppingApp
//
//  Created by Alima Aglakova on 05/02/2021.
//

import Foundation

struct Product {
    let ImageSource : String
    let Title : String
    let Description : String
    let Price : Double
}
