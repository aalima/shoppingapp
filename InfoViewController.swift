//
//  InfoViewController.swift
//  onlineShoppingApp
//
//  Created by Alima Aglakova on 05/02/2021.
//

import UIKit

class InfoViewController: UIViewController {
    static let identifier = String(describing: InfoViewController.self)
    @IBOutlet weak var infoImage: UIImageView!
    @IBOutlet weak var infoPrice: UILabel!
    @IBOutlet weak var infoTitle: UILabel!
    @IBOutlet weak var infoDescription: UILabel!
    
    public var item: Product?
    var index: Int = 0
    var delegate: ViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func didTapOnAddToCartInInfo(_ sender: Any) {
        delegate?.addToCart(index)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoImage.image = UIImage(named: item!.ImageSource)
        infoPrice.text = String(item!.Price)
        infoDescription.text = item!.Description
        infoTitle.text = item!.Title
    }
}
