//
//  MyCollectionViewCell.swift
//  onlineShoppingApp
//
//  Created by Alima Aglakova on 04/02/2021.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myCollectionImage: UIImageView!
    @IBOutlet weak var myCollectionPrice: UILabel!
    @IBOutlet weak var myCollectionDescription: UILabel!
    @IBOutlet weak var myCollectionTitle: UILabel!
    @IBOutlet weak var myCollectionButton: UIButton!
    var index: Int = 0
    var delegate: ViewControllerDelegate?

    static let identifier = String(describing: MyCollectionViewCell.self)
    static let nib = UINib(nibName: identifier, bundle: nil)

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func didTapAdd(_ sender: Any) {
        delegate?.addToCart(index)
    }
}
