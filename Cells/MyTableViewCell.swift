//
//  MyTableViewCell.swift
//  onlineShoppingApp
//
//  Created by Alima Aglakova on 04/02/2021.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    static let identifier = String(describing: MyTableViewCell.self)
    static let nib = UINib(nibName: identifier, bundle: nil)
    
    var index: Int = 0
    var delegate: ViewControllerDelegate?
    
    @IBOutlet weak var myTableCellButton: UIButton!
    @IBOutlet weak var myTableCellImage: UIImageView!
    @IBOutlet weak var myTableCellTitle: UILabel!
    @IBOutlet weak var myTableCellPrice: UILabel!
    @IBOutlet weak var MyTableCellDescription: UILabel!
    
    override func awakeFromNib(){
    super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func didTapAddToCartTableCell(_ sender: Any) {
        delegate?.addToCart(index)
    }
    
}
